// create map
const map = L.map('mapid').setView([-27.2214314,-49.6491719], 14);

// create and add a tileLayer
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

// create icon
const icon = L.icon({
    iconUrl: "./public/images/map-marker.svg",
    iconSize: [48,58],
    iconAnchor: [29, 68],
    popupAnchor: [140, 1]
})

// create popup overlay
const popup = L.popup({
    closeButton: false,
    className: 'map-popup',
    minWidth: 200,
    minHeight: 240
}).setContent(
    'Lar das meninas <a href="orphanage.html?id=1" class="choose-orphanage"> <img src="./public/images/arrow-white.svg"> </a>'
)

// create and add a marker
L.marker([-27.2191035,-49.6471119], { icon })
    .addTo(map)
    .bindPopup(popup)
